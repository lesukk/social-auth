﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;

namespace SocialAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "Vuukle",
                    ValidAudience = "Vuukle",
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes("xxxxxxxxxxxx-xxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxxxx"))
                };
            })
            .AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = "";// Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = "";// Configuration["Authentication:Facebook:AppSecret"];
                facebookOptions.Scope.Add("public_profile");
                facebookOptions.Scope.Add("email");
                facebookOptions.Events = new Microsoft.AspNetCore.Authentication.OAuth.OAuthEvents()
                {
                    OnRemoteFailure = exc =>
                    {
                        exc.Response.Redirect("/Auth/AuthFailed.html");
                        exc.HandleResponse();
                        Console.WriteLine("Issue caught on login in startup.cs file : {0}", exc.Failure.Message);
                        return System.Threading.Tasks.Task.FromResult(0);
                    }
                };
                //facebookOptions.SignInScheme = "Facebook";
            })
            .AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = "-.apps.googleusercontent.com";// Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = "";// Configuration["Authentication:Google:ClientSecret"];
                googleOptions.Events = new Microsoft.AspNetCore.Authentication.OAuth.OAuthEvents()
                {
                    OnRemoteFailure = exc =>
                    {
                        exc.Response.Redirect("/Auth/AuthFailed.html");
                        exc.HandleResponse();
                        Console.WriteLine("Issue caught on login in startup.cs file : {0}", exc.Failure.Message);
                        return System.Threading.Tasks.Task.FromResult(0);
                    }
                };
            })
            .AddTwitter(twitterOptions =>
            {
                twitterOptions.ConsumerKey = "";// Configuration["Authentication:Google:ClientId"];
                twitterOptions.ConsumerSecret = "";// Configuration["Authentication:Google:ClientSecret"];
                twitterOptions.RetrieveUserDetails = true;

                twitterOptions.Events = new Microsoft.AspNetCore.Authentication.Twitter.TwitterEvents()
                {
                    OnRemoteFailure = exc =>
                    {
                        exc.Response.Redirect("/Auth/AuthFailed.html");
                        exc.HandleResponse();
                        Console.WriteLine("Issue caught on login in startup.cs file : {0}", exc.Failure.Message);
                        return System.Threading.Tasks.Task.FromResult(0);
                    }
                };
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
