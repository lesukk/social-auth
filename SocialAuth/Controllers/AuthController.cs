﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Newtonsoft.Json.Linq;
using System.Net;

namespace SocialAuth.Controllers
{
    public class AuthController : Controller
    {
        private readonly SignInManager<ClaimsIdentity> _signInManager;
        public AuthController(SignInManager<ClaimsIdentity> signInManager)
        {
            _signInManager = signInManager;
        }
        [HttpGet]
        [Route("fbAuth")]
        public IActionResult RequestFBToken()
        {
            //HttpContext.Request.IsHttps ? "https" : "http"
            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Facebook", Url.Action("ExternalLoginCallback", "Auth"));
            return Challenge(properties, "Facebook");
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        
        [HttpGet]
        [Route("twAuth")]
        public IActionResult RequestTWToken()
        {

            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Twitter", Url.Action("ExternalLoginCallback", "Auth"));
            return Challenge(properties, "Twitter");
        }

        [ApiExplorerSettings(IgnoreApi = false)]
        [HttpGet]
        [Route("glAuth")]
        public IActionResult RequestGLToken()
        {
            //, null, HttpContext.Request.IsHttps ? "https" : "http"
            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Google", Url.Action("ExternalLoginCallback", "Auth"));
            return Challenge(properties, "Google");
        }
        
        [Route("ExternalLoginCallback")]
        public async Task<IActionResult> ExternalLoginCallback()
        {
            try
            {
                ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
                var p = info.Principal; //the IPrincipal with the claims from facebook
                                        //info.ProviderKey //an unique identifier from Facebook for the user that just signed in
                                        //info.LoginProvider //a string with the external login provider name, in this case Facebook
                var claims = p.Claims;

                var userid = info.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
                var name = info.Principal.FindFirstValue(ClaimTypes.Name);
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);

                if (string.IsNullOrWhiteSpace(userid) || string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(email))
                {
                    ViewData["Response"] = "0";
                    return View("~/Views/Auth/AuthResponse.cshtml");
                }
                var image = "";



                var data = new { name, email, image };
                ClaimsIdentity user = null;//_authService.Authenticate(email);
                if (user == null)
                {


                    if (info.LoginProvider == "Facebook")
                        image = string.Format("https://graph.facebook.com/v2.12/{0}/picture?height=50", userid);
                    else if (info.LoginProvider == "Google")
                    {
                        await Task.Run(() =>
                        {
                            var u = "https://www.googleapis.com/plus/v1/people/{0}?fields=image&key={1}";
                            try
                            {
                                using (WebClient client = new WebClient())
                                {
                                    var d = client.DownloadString(string.Format(u, userid, "GOOGLEAPI"));
                                    var response = JObject.Parse(d);
                                    var img = response["image"].Value<string>();
                                    if (!string.IsNullOrWhiteSpace(img))
                                        image = img;
                                }
                            }
                            catch { }
                        });
                    }
                    else if (info.LoginProvider == "Twitter")
                    {

                        await Task.Run(() =>
                        {
                            var u = "https://pbs.twimg.com/profile_images/{0}/7df3h38zabcvjylnyfe3_normal.png";
                            try
                            {
                                using (WebClient client = new WebClient())
                                {
                                    var d = client.DownloadString(string.Format(u, userid));
                                    var response = JObject.Parse(d);
                                    var img = response["image"].Value<string>();
                                    if (!string.IsNullOrWhiteSpace(img))
                                        image = img;
                                }
                            }
                            catch { }
                        });
                    }

                    //user = _userService.Create(new User { Name = name, Email = email, ApiKey = Guid.NewGuid().ToString(), EmailConfirmKey = Guid.NewGuid().ToString(), Roles = 0, PictureUrl = image });

                }
                //var authTicket = _authService.Authorize(user, null, true);

                return View("~/Views/Auth/AuthResponse.cshtml","" /*authTicket.Token*/);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Issue happened on login: {0}", ex.Message);

            }
            return View("~/Views/Auth/AuthResponse.cshtml", "");
        }
    }
}